SDP Prototype
=============

This repository contains a set of packages for deploying a minimal SDP
system capable of configuring and executing workflows.

.. toctree::
  :maxdepth: 1

  getting_started

.. toctree::
  :maxdepth: 1
  :caption: SDP Prototype Design

  design/design
  design/components
  design/module

.. toctree::
  :maxdepth: 1
  :caption: Running the SDP Prototype

  running/setting_up_local_dev_env
  running/running_standalone
  running/running_integration

.. toctree::
  :maxdepth: 1
  :caption: Building and testing

  building/building_tango_devices
  building/testing_tango_devices

.. toctree::
  :maxdepth: 1
  :caption: Tango Devices

  tango_devices/sdp_master
  tango_devices/sdp_subarray

.. toctree::
  :maxdepth: 1
  :caption: Configuration

  configuration/config_db
  configuration/config_schema
  configuration/config_api

.. toctree::
  :maxdepth: 1
  :caption: Services

  services/processing_controller
  services/helm_deployer

.. toctree::
  :maxdepth: 1
  :caption: Workflows

  workflows/workflows

Indices and tables
------------------

- :ref:`genindex`
- :ref:`modindex`
- :ref:`search`
