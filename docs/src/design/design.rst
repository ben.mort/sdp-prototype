Design Overview
===============

Introduction
------------

This prototype is a partial implementation of the SDP software architecture
adopted by the SKA. Its purpose is to implement and test parts of the
architecture to de-risk the construction of the SDP.

The most recent version of the complete SDP architecture can be found in
the `SDP Consortium close-out documentation
<http://ska-sdp.org/publications/sdp-cdr-closeout-documentation>`_. The
architecture is intended to be a living document that evolves alongside its
implementation, so it will eventually be available in a form that can more
readily be changed.